################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../STemWinLibrary522_4x9i/Config/GUIConf.c \
../STemWinLibrary522_4x9i/Config/GUIDRV_stm32f429i_discovery.c 

OBJS += \
./STemWinLibrary522_4x9i/Config/GUIConf.o \
./STemWinLibrary522_4x9i/Config/GUIDRV_stm32f429i_discovery.o 

C_DEPS += \
./STemWinLibrary522_4x9i/Config/GUIConf.d \
./STemWinLibrary522_4x9i/Config/GUIDRV_stm32f429i_discovery.d 


# Each subdirectory must supply rules for building sources it contributes
STemWinLibrary522_4x9i/Config/%.o: ../STemWinLibrary522_4x9i/Config/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Sourcery Windows GCC C Compiler'
	arm-none-eabi-gcc -DSTM32F429_439xx -D__VFP_FP__ -DUSE_STDPERIPH_DRIVER -DUSE_STM32F429I_DISCO -I"C:\ARM\workspace\stm32f429_lcd2\base" -I"C:\ARM\workspace\stm32f429_lcd2\User_HandsOn" -I"C:\ARM\workspace\stm32f429_lcd2\STemWinLibrary522_4x9i\inc" -I"C:\ARM\workspace\stm32f429_lcd2\STemWinLibrary522_4x9i\Config" -I"C:\ARM\workspace\stm32f429_lcd2\lib" -I"C:\ARM\workspace\stm32f429_lcd2\STM32F4xx_StdPeriph_Driver\inc" -O0 -ffunction-sections -fdata-sections -Wall -std=gnu99 -Wa,-adhlns="$@.lst" -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


