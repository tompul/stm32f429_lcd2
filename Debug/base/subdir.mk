################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../base/syscalls.c \
../base/system_stm32f4xx.c 

OBJS += \
./base/syscalls.o \
./base/system_stm32f4xx.o 

C_DEPS += \
./base/syscalls.d \
./base/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
base/%.o: ../base/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Sourcery Windows GCC C Compiler'
	arm-none-eabi-gcc -DSTM32F429_439xx -DUSE_STDPERIPH_DRIVER -DUSE_STM32F429I_DISCO -I"C:\ARM\workspace\stm32f429_lcd2\base" -I"C:\ARM\workspace\stm32f429_lcd2\User_HandsOn" -I"C:\ARM\workspace\stm32f429_lcd2\STemWinLibrary522_4x9i\inc" -I"C:\ARM\workspace\stm32f429_lcd2\STemWinLibrary522_4x9i\Config" -I"C:\ARM\workspace\stm32f429_lcd2\lib" -I"C:\ARM\workspace\stm32f429_lcd2\STM32F4xx_StdPeriph_Driver\inc" -O0 -ffunction-sections -fdata-sections -Wall -Wa,-adhlns="$@.lst" -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=vfpv4 -g3 -gdwarf-2 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


